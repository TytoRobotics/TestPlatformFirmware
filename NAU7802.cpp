#include "NAU7802.h"
#include <Arduino.h>
#include "I2C.h"
#include "Error.h"
extern byte is1580;
extern byte is1585;

// CONSTRUCTOR
NAU7802::NAU7802(byte adc_num){
	//Select one of the ADC (do not leave floating)
  pinMode(A0, OUTPUT); //analog switch ADC selector (they all have the same I2C address).
  pinMode(A3, OUTPUT); 
	_adc_num = adc_num;
	_adc_val_step = 0;
	_calibrate_step = 0;
}

//Call this function until it returns true (avoid busy waits)
byte NAU7802::get_adc_val(float *value, byte channel){
	switch (_adc_val_step){
	case 0:
		if(_selectChannel(channel, _gain[channel], 0)){
      _average_buffer = 0.0;
      _average_buffer_size = 0;
      _adc_val_step++;
		} 
		break;
	case 1: //rotating filter
		if (_adc_update(&_adc_voltage,&_adc_value)){
      _rotating_filter[channel][0] = _rotating_filter[channel][1];
      _rotating_filter[channel][1] = _rotating_filter[channel][2];
      _rotating_filter[channel][2] = _adc_voltage;
      
      _adc_val_step++; 
		}
		break;
	case 2:
		//Dynamic gain adjustment
		if (abs(_adc_value) > 2.45 && _gain[channel] > 1){
			//Decrease the gain
			_new_gain = _gain[channel] / 2;
			_adc_val_step++;
		}else{
			if (abs(_adc_value) < 0.5 && _gain[channel] < 128){
				//Increase the gain
				_new_gain = _gain[channel] * 2;
				_adc_val_step++;
			}else{
				_adc_val_step = 0;

        // apply median filter to get answer
        float median;
        if (_rotating_filter[channel][2] < _rotating_filter[channel][1]) {
          if (_rotating_filter[channel][2] < _rotating_filter[channel][0]) {
            if (_rotating_filter[channel][1] < _rotating_filter[channel][0]) {
              median = _rotating_filter[channel][1];
            }
            else {
              median = _rotating_filter[channel][0];
            }
          }
          else {
            median = _rotating_filter[channel][2];
          }
        }
        else {
          if (_rotating_filter[channel][2] < _rotating_filter[channel][0]) {
            median = _rotating_filter[channel][2];
          }
          else {
            if (_rotating_filter[channel][1] < _rotating_filter[channel][0]) {
              median = _rotating_filter[channel][0];
            }
            else {
              median = _rotating_filter[channel][1];
            }
          }
        }
        *value = median;
				return true;
			}
		}
		break;
	case 3:
		//Change the gain if a dynamic adjustment is required
		if (_selectChannel(channel, _new_gain, 0)) {
			_adc_val_step=0;
		}
		break;
	}
	return false;
}

//Select adc relative to declared object
#define NOP __asm__ __volatile__ ("nop\n\t") // delay 62.5ns on a 16MHz AtMega
void NAU7802::_ADC_select(void){
  // ADC 0 is for 1520, 1580, 1585 
  // ADC 1 is for 1580, 1585
  // ADC 2 is for 1585
  /* Truth table (X=don't care):
     sA0   sA3
      1     1    ADC0 (Thrust)
      0     1    ADC1 (Left)
      X     0    ADC2 (Right)
  */
  bool sA0 = _adc_num & 0x01;
  bool sA3 = _adc_num >= 2;
	if(digitalRead(A0)!=sA0 || digitalRead(A3)!=sA3){
		TWCR = 0; //releases SDA and SCL lines to high impedance
		NOP; NOP; NOP;
		//Analog Switch IC connected to this pin (because this ADC chip has only one I2C address, and we use two)
		while (digitalRead(A0)!=sA0 || digitalRead(A3)!=sA3){
			pinMode(A0, OUTPUT);
      pinMode(A3, OUTPUT);
			digitalWrite(A0, sA0);
      digitalWrite(A3, sA3);
		}
		NOP; NOP; NOP;
		TWCR = _BV(TWEN) | _BV(TWEA); //reinitialize TWI 
		NOP; NOP; NOP;
	}
}

//Performs the internal on-chip calibration
//Call this function until it returns true (avoid busy waits)
byte NAU7802::_calibrate(void){
	_ADC_select();
	switch (_calibrate_step) {
	case 0:
		_calibrate_ctrl2_reg = readRegister(NAU7802_h_I2C_Address, CTRL2);
		bitSet(_calibrate_ctrl2_reg, CALS);
		writeRegister(NAU7802_h_I2C_Address, CTRL2, _calibrate_ctrl2_reg);
		_calibrate_step++;
		break;
	case 1:
		//Keep doing this until calibration complete
		if (bitRead(_calibrate_ctrl2_reg, CALS)) {
			_calibrate_ctrl2_reg = readRegister(NAU7802_h_I2C_Address, CTRL2);
		}else{
			if (bitRead(_calibrate_ctrl2_reg, CAL_ERR)){
				Serial.println("***CAL ERROR***");
				_calibrate_step = 0;
				return false; //redo calibration
			}
			return true;
		}
		break;
	}
	return false;
}

// INITIALIZATION
// Returns a 0 if communication failed, 1 if successful.
byte NAU7802::init()
{
	_ADC_select();

	//Turn on device according to datasheet power-on sequencing (section 9.1)
  byte register_val = 0x00;
	bitSet(register_val,RR); //Perform reset
	writeRegister(NAU7802_h_I2C_Address,PU_CTRL,register_val); 
	bitClear(register_val,RR);
	bitSet(register_val,PUD_); //Turn on digital circuit
	writeRegister(NAU7802_h_I2C_Address,PU_CTRL,register_val); 
	long timeout = millis();
	while(!bitRead(register_val,PUR)){
		if (millis() - timeout > 100){
			error(NAU7802_Init);
			return false;
		}
		register_val = readRegister(NAU7802_h_I2C_Address,PU_CTRL);
	}

	//Turn on analog circuit
	bitSet(register_val,PUA);
	bitSet(register_val,CS);
	writeRegister(NAU7802_h_I2C_Address,PU_CTRL,register_val); 

	//Turn off the chopper function and enable strong pullup
	writeRegister(NAU7802_h_I2C_Address,OTP_B1,0x30);
	writeRegister(NAU7802_h_I2C_Address,I2C_Control,0<<BGPCP | 1<<WPD | 1<<SPE_ | 0<<TS | 0<<BOPGA);
	writeRegister(NAU7802_h_I2C_Address,CTRL1,0<<GAINS_0);
	writeRegister(NAU7802_h_I2C_Address,CTRL2,0<<CHS | 0<<CRS_2 | 1<<CRS_1 | 1<<CRS_0); //Channel select, rate 80sps
	
	//Set initial gain for each channel
	timeout = millis();
	while (!_selectChannel(1, 128, 1)){
		if (millis() - timeout > 500){
			error(NAU7802_Init);
			return false;
		}
	};
	while (!_selectChannel(0, 128, 1)){
		if (millis() - timeout > 1000){
			error(NAU7802_Init);
			return false;
		}
	};
	return 1;
}

//Set the adc channel and the gain. Calibrate as recommended by datasheet.
//Call this function until it returns true (avoid busy waits)
byte NAU7802::_selectChannel(byte newchannel, byte gain, byte force){
	byte gain_counter, i2c_control, ctrl2, ctrl1;
  float a,b; // dummy values
	if (newchannel != _channel || gain != _gain[newchannel] || _channel_select_step>0 || force){
		_ADC_select();
		switch (_channel_select_step){
		case 0:
			i2c_control = readRegister(NAU7802_h_I2C_Address,I2C_Control);
			ctrl2 = readRegister(NAU7802_h_I2C_Address,CTRL2);
			bitWrite(i2c_control,TS,newchannel==2);
			bitWrite(ctrl2,CHS,newchannel&0x01);
			writeRegister(NAU7802_h_I2C_Address,I2C_Control,i2c_control);
			writeRegister(NAU7802_h_I2C_Address,CTRL2,ctrl2);
			_channel = newchannel;

			//Calculate the gain index (0 to 7) based on the actual gain (1 to 128)
			_gain_index[newchannel] = 0;
			gain_counter = gain;
			while (gain_counter>1){
				gain_counter = gain_counter >> 1;
				_gain_index[newchannel]++;
			}

			//Set the new gain
			ctrl1 = readRegister(NAU7802_h_I2C_Address, CTRL1);
			bitWrite(ctrl1, GAINS_2, bitRead(_gain_index[newchannel], 2));
			bitWrite(ctrl1, GAINS_1, bitRead(_gain_index[newchannel], 1));
			bitWrite(ctrl1, GAINS_0, bitRead(_gain_index[newchannel], 0));
			writeRegister(NAU7802_h_I2C_Address, CTRL1, ctrl1);
			_gain[newchannel] = gain;

     //Force multiple samples before taking result
     _conversion_settling = NAU7802_h_STABLE_CONV;
				
			_channel_select_step++;
			break;
    case 1:
      if(_conversion_settling > 0){
        if(_adc_update(&a, &b)){
          _conversion_settling--;
        }
      }else{
        _channel_select_step++;
      }
      break;
		case 2:
			if (_calibrate()){ //Calibrate
				_channel_select_step = 0;
				return true;
			}
			break;
		}
		return false;
	}else{
		return true;
	}
}


#define BITS_TO_V 0.000000298023223876953125
const float gain_mult[] = { 
	1.0, 
	1.0 / 2.0, 
	1.0 / 4.0,
	1.0 / 8.0,
	1.0 / 16.0,
	1.0 / 32.0,
	1.0 / 64.0,
	1.0 / 128.0 }; //Precompiled divisions for performance

//Updates results. voltageValue is the input real voltage, adcVoltage is the voltage*gain as read by the ADC.
byte NAU7802::_adc_update(float *voltageValue, float *adcVoltage){
	_ADC_select();
	byte control_register = readRegister(NAU7802_h_I2C_Address,PU_CTRL);
	uint8_t u8ADCresult[3];
	int32_t	u32ADCresult;
	
	if(bitRead(control_register,CR)){ //New reading available
		u8ADCresult[2] = readRegister(NAU7802_h_I2C_Address,ADCO_B2);
		u8ADCresult[1] = readRegister(NAU7802_h_I2C_Address,ADCO_B1);
		u8ADCresult[0] = readRegister(NAU7802_h_I2C_Address,ADCO_B0);

		u32ADCresult=u8ADCresult[2];
		u32ADCresult=(u32ADCresult<<8) + u8ADCresult[1];
		u32ADCresult=(u32ADCresult<<8) + u8ADCresult[0];
		if (u8ADCresult[2]>127)
		{
			u32ADCresult|=0xFF000000;
		}	
	
		//Convert to equivalent voltage (assuming radiometric 5V)
		*adcVoltage = ((float)u32ADCresult)*BITS_TO_V;
		*voltageValue = *adcVoltage*gain_mult[_gain_index[_channel]];

		//If it is a temperature convert the voltage to C, using the datasheet's info.
		/*if(_channel==2){
			*voltageValue = 25 + (*voltageValue - 0.109)*(1 / 0.000360);
		}*/
		return true;
	}
	return false;
}
