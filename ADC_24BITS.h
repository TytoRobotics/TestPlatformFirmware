#ifndef ADC_24BITS_h
#define ADC_24BITS_h

#include <Arduino.h>
#include "NAU7802.h" // 24-bit ADCs

class ADC_24BITS
{
public:	
	ADC_24BITS(); // Constructor
	void init(bool is1580, bool is1585);
	byte get_adc(byte adc_num, byte channel, float *value);
	byte get_ohms(byte is1585, float *value, byte *result_state);
private:
	float _get_load_cell(float adc_offset, float adc_value);
	byte _adc0_channel_lock;
};

#endif
