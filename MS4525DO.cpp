#include "MS4525DO.h"
#include <Arduino.h>
#include "I2C.h"
#include "Error.h"

// CONSTRUCTOR
MS4525DO::MS4525DO() {
    
}

byte MS4525DO::init()
{
    read();
    return(0);
}

byte MS4525DO::read()
{
  byte _status; //Interfacing_to_MEAS_Digital_Pressure_Modules p5
  byte Press_H, Press_L;
  unsigned int P_dat;
  unsigned int T_dat;

  byte notConnected;

  byte rawData[4];  // x/y/z accel register data stored here
  // Read the 4 raw data registers into data array
  notConnected = I2c.read(MS4525DO_h_I2C_Address, 4, rawData);  
  Press_H = rawData[0];
  Press_L = rawData[1];
  byte Temp_H = rawData[2];
  byte  Temp_L = rawData[3];

  _status = (Press_H >> 6) & 0x03;
  Press_H = Press_H & 0x3f;
    
  if (!notConnected) { // If connected {
    pressure_P = (((unsigned int)Press_H) << 8) | Press_L;

    Temp_L = (Temp_L >> 5);
    pressure_T = (((unsigned int)Temp_H) << 3) | Temp_L;
    board_info.pSensorAvailable = 1;
  }
  else {
    pressure_P = 0;
    pressure_T = 0;
    board_info.pSensorAvailable = 0;
  }

  
    
    // = _status;
   
  
  return (_status); //Interfacing_to_MEAS_Digital_Pressure_Modules p5
  return (0);
}

byte MS4525DO::available()
{
  return(board_info.pSensorAvailable);
}
