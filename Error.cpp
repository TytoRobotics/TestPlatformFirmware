#include <Arduino.h>
#include "Error.h"
#include "types.h"
#include "RC_SIGNALS.h" //for cutting off

extern RC_SIGNALS rc;

//For now, the error simply stops the program on the red led and displays the error message.
void error(byte error_code){
	//Turn off motor if it was on...
	rc.set(1000, 0);
	
	//Turn on the red LED
	pinMode(13, 1); //Set as output
	digitalWrite(13, 1);

	//Displays the message on screen
	switch (error_code){
	case I2Cbus:
		Serial.println("I2C bus locked");
		break;
	case AccNoDetect:
		Serial.println("Acc not detected");
		break;
	case NAU7802_Init:
		Serial.println("Could not init NAU7802");
		break;
  case ISL28022_Init:
    Serial.println("Could not init ISL28022");
    break;
  case ISL28022_Overflow:
    Serial.println("Power Chip Overflow!");
    break;
	}

	//Freeze there
	while(1){
		delay(50);
		digitalWrite(13, 0);
		delay(100);
		digitalWrite(13, 1);
	}
}

void red_led(byte value){
	pinMode(13, 1); //Set as output
	digitalWrite(13, value);
}
