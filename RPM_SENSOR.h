#ifndef RPM_SENSOR_h
#define RPM_SENSOR_h

#include <Arduino.h>

class RPM_SENSOR
{
public:	
	RPM_SENSOR(); // Constructor
	void init(void);
	void print(void);
	float read_brushless_hz();
private:

};

#endif
