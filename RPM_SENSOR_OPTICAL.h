#ifndef RPM_SENSOR_OPTICAL_h
#define RPM_SENSOR_OPTICAL_h


#include <Arduino.h>

class RPM_SENSOR_OPTICAL
{
public:  
  RPM_SENSOR_OPTICAL(); // Constructor
  void init(void);
  void print(void);
  float read_hz();
private:
};

#endif
